package ru.anisimov.squares;

import ru.anisimov.squares.controller.ImageSettingsController;
import ru.anisimov.squares.model.ImageSettingsModel;
import ru.anisimov.squares.view.ImageSettingsForm;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.image.BufferedImage;
import java.io.IOException;

import static javax.swing.SwingUtilities.invokeLater;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         02.03.17
 */
public class Launcher {
    private static void createAndShowGUI() {
        ImageSettingsModel model = new ImageSettingsModel();
        ImageSettingsController controller = new ImageSettingsController(model);
        ImageSettingsForm view = new ImageSettingsForm(controller, model);
        model.addListener(view);

        JFrame frame = new JFrame("Рисуем по квадратам");
        frame.setContentPane(view.mainPanel);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
        try {
            BufferedImage image = ImageIO.read(
                Launcher.class.getClassLoader().getResourceAsStream("paint-brush-hand-drawn-tool.png"));
            frame.setIconImage(image);
        } catch (IOException ignored) {
            ignored.printStackTrace();
        }
        frame.pack();
        frame.setVisible(true);
        //frame.setResizable(false);

        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
            @Override
            public void uncaughtException(Thread t, Throwable e) {
                JOptionPane.showMessageDialog(null,
                    e.getMessage() == null || e.getMessage().isEmpty()
                        ? "Неизвестная ошибка" : e.getMessage(), "Ошибка!", JOptionPane.ERROR_MESSAGE);
            }
        });
    }

    public static void main(String[] args) {
        invokeLater(new Runnable() {
            @Override
            public void run() {
                Launcher.createAndShowGUI();
            }
        });
    }
}

package ru.anisimov.squares.controller;

import ru.anisimov.squares.model.Grid;
import ru.anisimov.squares.model.ImageSettingsModel;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         02.03.17
 */
public class ImageSettingsController {
    private ImageSettingsModel model;

    public ImageSettingsController(ImageSettingsModel model) {
        this.model = model;
    }

    public void loadImageFromUrl(String url) {
        try {
            BufferedImage image = ImageIO.read(new URL(url));
            model.setImage(image);
        } catch (IOException e) {
            throw new RuntimeException("Невозможно загрузить картинку по указанному адресу", e);
        }
    }

    public void clearImage() {
        model.setImage(null);
    }

    public void createGrid(Grid grid) {
        model.setGrid(grid);
    }

    public void clearGrid() {
        model.setGrid(null);
    }
}

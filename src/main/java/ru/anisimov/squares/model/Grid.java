package ru.anisimov.squares.model;

import java.awt.*;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         02.03.17
 */
public class Grid {
    private int widthCells;
    private int heightCells;

    private Color color = Color.white;

    public Grid(int widthCells, int heightCells) {
        this.widthCells = widthCells;
        this.heightCells = heightCells;
    }

    public Grid(int widthCells, int heightCells, Color color) {
        this.widthCells = widthCells;
        this.heightCells = heightCells;
        this.color = color;
    }

    public int getWidthCells() {
        return widthCells;
    }

    public void setWidthCells(int widthCells) {
        this.widthCells = widthCells;
    }

    public int getHeightCells() {
        return heightCells;
    }

    public void setHeightCells(int heightCells) {
        this.heightCells = heightCells;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }
}

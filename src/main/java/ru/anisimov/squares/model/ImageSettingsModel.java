package ru.anisimov.squares.model;

import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         02.03.17
 */
public class ImageSettingsModel {
    private BufferedImage image;
    private Grid grid;

    public BufferedImage getImage() {
        return image;
    }

    public Grid getGrid() {
        return grid;
    }

    public void setImage(BufferedImage image) {
        this.image = image;
        for (Listener listener : listeners) {
            listener.imageChanged(image);
        }
    }

    public void setGrid(Grid grid) {
        this.grid = grid;
        for (Listener listener : listeners) {
            listener.gridChanged(grid);
        }
    }


    private List<Listener> listeners = new ArrayList<>();

    public void addListener(Listener listener) {
        listeners.add(listener);
    }

    public void removeListener(Listener listener) {
        listeners.remove(listener);
    }

    public interface Listener {
        void imageChanged(BufferedImage image);
        void gridChanged(Grid grid);
    }
}

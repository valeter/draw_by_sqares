package ru.anisimov.squares.util;

import java.awt.*;
import java.awt.image.BufferedImage;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         02.03.17
 */
public class ImageProcessor {
    public static BufferedImage scaleAndCrop(BufferedImage image, int newWidth, int newHeight) {
        try {
            int newScaledWidth = (double) image.getHeight() / image.getWidth() > (double) newHeight / newWidth ? newWidth : Math.round((float) image.getWidth() * newHeight / (float) image.getHeight());
            int newScaledHeight = (double) image.getWidth() / image.getHeight() > (double) newWidth / newHeight ? newHeight : Math.round((float) image.getHeight() * newWidth / (float) image.getWidth());
            int width = newScaledWidth == newWidth ? image.getWidth() : image.getHeight() * newWidth / newHeight;
            int height = newScaledHeight == newHeight ? image.getHeight() : image.getWidth() * newHeight / newWidth;
            int x = (image.getWidth() - width) / 2;
            int y = (image.getHeight() - height) / 2;
            Image resizedImage = image.getSubimage(x, y, width, height).getScaledInstance(newWidth, newHeight, Image.SCALE_SMOOTH);
            return drawImage(resizedImage, newWidth, newHeight);
        } catch (Exception e) {
            throw new RuntimeException("Невозможно изменить размер изображения!", e);
        }
    }

    public static BufferedImage crop(BufferedImage image, int x, int y, int width, int height) {
        Image croppedImage = image.getSubimage(x, y, width, height);
        return drawImage(croppedImage, width, height);
    }

    private static BufferedImage drawImage(Image image, int width, int height) {
        BufferedImage imageBuff = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        Graphics g = imageBuff.createGraphics();
        g.drawImage(image, 0, 0, width, height, null);
        g.dispose();
        return imageBuff;
    }
}

package ru.anisimov.squares.view;

import ru.anisimov.squares.controller.ImageSettingsController;
import ru.anisimov.squares.model.Grid;
import ru.anisimov.squares.model.ImageSettingsModel;
import ru.anisimov.squares.util.ImageProcessor;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.concurrent.atomic.AtomicReference;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         02.03.17
 */
public class ImageSettingsForm implements ImageSettingsModel.Listener {
    private static final int DEFAULT_IMAGE_SCALE_COEFFICIENT = 60;
    private static final int MAX_IMAGE_SCALE_COEFFICIENT = 500;
    private static final int NO_IMAGE_SCALE_COEFFICIENT = 300;
    private static final int MIN_IMAGE_SCALE_COEFFICIENT = 10;
    private static final int IMAGE_SCALE_COEFFICIENT_STEP = 20;

    public JPanel mainPanel;
    private JTextField imageUrl;
    private JButton downloadImageButton;
    private JButton clearAllButton;
    private JButton saveButton;
    private JTextField gridHeightText;
    private JTextField gridWidthText;
    private JButton clearGridButton;
    private JButton createGridButton;
    private JButton imageZoomIn;
    private JButton imageZoomOut;
    private JPanel imagePanel;
    private JComboBox gridColor;
    private JButton fitByGridButton;

    private ImageSettingsController controller;
    private ImageSettingsModel model;

    private int imageScaleCoefficient = DEFAULT_IMAGE_SCALE_COEFFICIENT;
    private int imageOffsetX = 0;
    private int imageOffsetY = 0;

    private int mouseDragStartX = 0;
    private int mouseDragStartY = 0;


    public ImageSettingsForm(ImageSettingsController controller, ImageSettingsModel model) {
        this.controller = controller;
        this.model = model;
        initButtons();
    }

    private void initPanel() {
    }

    private void initButtons() {
        createGridButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                try {
                    int width = Integer.parseInt(gridWidthText.getText());
                    int height = Integer.parseInt(gridHeightText.getText());
                    Color color;
                    int selectedColorInd = gridColor.getSelectedIndex();
                    switch (selectedColorInd) {
                        case 1:
                            color = Color.black;
                            break;
                        case 2:
                            color = Color.red;
                            break;
                        case 3:
                            color = Color.blue;
                            break;
                        case 4:
                            color = Color.green;
                            break;
                        case 0:
                        default:
                            color = Color.white;
                            break;
                    }
                    controller.createGrid(new Grid(width, height, color));
                } catch (NumberFormatException e) {
                    throw new RuntimeException("Неверно введены высота и ширина сетки!", e);
                }
            }
        });

        clearGridButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                controller.clearGrid();
            }
        });

        downloadImageButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                clearAll();
                downloadImageButton.setEnabled(false);
                try {
                    controller.loadImageFromUrl(imageUrl.getText());
                } finally {
                    downloadImageButton.setEnabled(true);
                }
            }
        });

        imageZoomIn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                imageScaleCoefficient = Math.min(MAX_IMAGE_SCALE_COEFFICIENT, imageScaleCoefficient + IMAGE_SCALE_COEFFICIENT_STEP);
                repaintImage();
            }
        });

        imageZoomOut.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                imageScaleCoefficient = Math.max(MIN_IMAGE_SCALE_COEFFICIENT, imageScaleCoefficient - IMAGE_SCALE_COEFFICIENT_STEP);
                repaintImage();
            }
        });

        MouseAdapter imageDragListener = new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                mouseDragStartX = e.getX();
                mouseDragStartY = e.getY();
            }

            @Override
            public void mouseDragged(MouseEvent e) {
                int dx = e.getX() - mouseDragStartX;
                int dy = e.getY() - mouseDragStartY;
                imageOffsetX += dx;
                imageOffsetY += dy;
                mouseDragStartX = e.getX();
                mouseDragStartY = e.getY();
                repaintImage();
            }
        };
        imagePanel.addMouseListener(imageDragListener);
        imagePanel.addMouseMotionListener(imageDragListener);

        clearAllButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                clearAll();
            }
        });

        fitByGridButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Grid grid = model.getGrid();
                if (grid == null) {
                    return;
                }
                int imagePanelWidth = imagePanel.getWidth();
                int imagePanelHeight = imagePanel.getHeight();
                imageOffsetY = getGridOffsetY(grid, imagePanelWidth, imagePanelHeight);
                imageOffsetX = getGridOffsetX(grid, imagePanelWidth, imagePanelHeight);
                repaintImage();
            }
        });

        saveButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                if (model.getImage() == null) {
                    return;
                }
                JFileChooser fileChooser = new JFileChooser();
                BufferedImage image = renderImageForSave();
                if (image != null && fileChooser.showSaveDialog(mainPanel) == JFileChooser.APPROVE_OPTION) {
                    File file = fileChooser.getSelectedFile();
                    try {
                        ImageIO.write(image, "png", file);
                    } catch (IOException e) {
                        throw new RuntimeException("Невозможно сохранить файл!", e);
                    }
                }
            }
        });
    }

    private void clearAll() {
        controller.clearGrid();
        controller.clearImage();
        imageOffsetX = 0;
        imageOffsetY = 0;
        imageScaleCoefficient = DEFAULT_IMAGE_SCALE_COEFFICIENT;
    }

    private BufferedImage renderImageForSave() {
        int imageWidth = getImageWidth(model.getImage());
        int imageHeight = getImageHeight(model.getImage());

        int gridOffsetX = getGridOffsetX(model.getGrid(), imagePanel.getWidth(), imagePanel.getHeight());
        int gridOffsetY = getGridOffsetY(model.getGrid(), imagePanel.getWidth(), imagePanel.getHeight());
        int cellSize = getCellSize(model.getGrid(), imagePanel.getWidth(), imagePanel.getHeight());
        int gridWidth = getGridSizeWidth(model.getGrid(), cellSize);
        int gridHeight = getGridSizeHeight(model.getGrid(), cellSize);

        int croppedViewOffsetX = Math.max(imageOffsetX, gridOffsetX);
        int croppedViewWidth = Math.min(imageOffsetX + imageWidth, gridOffsetX + gridWidth) - croppedViewOffsetX;
        int croppedViewOffsetY = Math.max(imageOffsetY, gridOffsetY);
        int croppedViewHeight = Math.min(imageOffsetY + imageHeight, gridOffsetY + gridHeight) - croppedViewOffsetY;

        if (croppedViewWidth <= 0 || croppedViewHeight <= 0) {
            throw new RuntimeException("Поместите изображение под сетку!");
        }

        double viewToModelScaleCoeff = (double) model.getImage().getWidth() / imageWidth;

        int croppedModelOffsetX = (int) ((croppedViewOffsetX - imageOffsetX) * viewToModelScaleCoeff);
        int croppedModelOffsetY = (int) ((croppedViewOffsetY - imageOffsetY) * viewToModelScaleCoeff);
        int croppedModelWidth = (int) (croppedViewWidth * viewToModelScaleCoeff);
        int croppedModelHeight = (int) (croppedViewHeight * viewToModelScaleCoeff);

        BufferedImage image = model.getImage();
        BufferedImage result = ImageProcessor.crop(image,
            Math.max(0, croppedModelOffsetX), Math.max(0, croppedModelOffsetY),
            Math.min(image.getWidth(), croppedModelWidth), Math.min(image.getHeight(), croppedModelHeight));

        int gridResultOffsetX = (int) ((gridOffsetX - croppedViewOffsetX) * viewToModelScaleCoeff);
        int gridResultOffsetY = (int) ((gridOffsetY - croppedViewOffsetY) * viewToModelScaleCoeff);
        int gridCellSizeModel = (int) (cellSize * viewToModelScaleCoeff);
        paintGrid(model.getGrid(), result.getGraphics(), gridCellSizeModel, gridResultOffsetX, gridResultOffsetY);
        return result;
    }

    private AtomicReference<BufferedImage> bufferReference = new AtomicReference<>();

    private void repaintImage() {
        BufferedImage buffer = bufferReference.get();
        if (buffer == null) {
            buffer = new BufferedImage(imagePanel.getWidth(), imagePanel.getHeight(), BufferedImage.TYPE_INT_RGB);
            bufferReference.compareAndSet(null, buffer);
        }
        Graphics result = imagePanel.getGraphics();

        boolean hasImage = true;
        outer:
        {
            BufferedImage image = model.getImage();
            if (image == null) {
                hasImage = false;
                break outer;
            }
            int width = getImageWidth(image);
            int height = getImageHeight(image);
            image = ImageProcessor.scaleAndCrop(image, width, height);

            Graphics bufferGraphics = buffer.getGraphics();
            ((Graphics2D) bufferGraphics).setBackground(new Color(125, 125, 125));
            bufferGraphics.clearRect(0, 0, imagePanel.getWidth(), imagePanel.getHeight());
            bufferGraphics.drawImage(image, imageOffsetX, imageOffsetY, null);

            Grid grid = model.getGrid();
            if (grid != null) {
                int cellSize = getCellSize(grid, imagePanel.getWidth(), imagePanel.getHeight());
                int offsetX = getGridOffsetX(grid, imagePanel.getWidth(), imagePanel.getHeight());
                int offsetY = getGridOffsetY(grid, imagePanel.getWidth(), imagePanel.getHeight());
                paintGrid(grid, bufferGraphics, cellSize, offsetX, offsetY);
            }
        }

        if (hasImage) {
            result.drawImage(buffer, 0, 0, imagePanel.getWidth(), imagePanel.getHeight(), null);
        } else {
            ((Graphics2D) result).setBackground(new Color(125, 125, 125));
            result.clearRect(0, 0, imagePanel.getWidth(), imagePanel.getHeight());
        }
    }

    private boolean gridBoundByWidth(Grid grid, int imagePanelWidth, int imagePanelHeight) {
        boolean imagePanelVertical = imagePanelWidth < imagePanelHeight;
        boolean gridVertical = grid.getWidthCells() < grid.getHeightCells();
        return imagePanelVertical && !gridVertical;
    }

    private int getImageWidth(BufferedImage image) {
        return image.getWidth() * imageScaleCoefficient / NO_IMAGE_SCALE_COEFFICIENT;
    }

    private int getImageHeight(BufferedImage image) {
        return image.getHeight() * imageScaleCoefficient / NO_IMAGE_SCALE_COEFFICIENT;
    }

    private int getCellSize(Grid grid, int imagePanelWidth, int imagePanelHeight) {
        if (gridBoundByWidth(grid, imagePanelWidth, imagePanelHeight)) {
            return (imagePanelWidth - ((grid.getWidthCells() + 1))) / grid.getWidthCells();
        } else {
            return (imagePanelHeight - ((grid.getHeightCells() + 1))) / grid.getHeightCells();
        }
    }

    private int getGridSizeWidth(Grid grid, int cellSize) {
        return (grid.getWidthCells() + 1) // lines
            + grid.getWidthCells() * cellSize; // cells
    }

    private int getGridSizeHeight(Grid grid, int cellSize) {
        return (grid.getHeightCells() + 1) // lines
            + grid.getHeightCells() * cellSize; // cells
    }

    private int getGridOffsetX(Grid grid, int imagePanelWidth, int imagePanelHeight) {
        int offsetX = 0;
        if (!gridBoundByWidth(grid, imagePanelWidth, imagePanelHeight)) {
            offsetX = (imagePanelWidth - getGridSizeWidth(grid, getCellSize(grid, imagePanelWidth, imagePanelHeight))) / 2;
        }
        return offsetX;
    }

    private int getGridOffsetY(Grid grid, int imagePanelWidth, int imagePanelHeight) {
        int offsetY = 0;
        if (gridBoundByWidth(grid, imagePanelWidth, imagePanelHeight)) {
            offsetY = (imagePanelHeight - getGridSizeHeight(grid, getCellSize(grid, imagePanelWidth, imagePanelHeight))) / 2;
        }
        return offsetY;
    }

    private void paintGrid(Grid grid, Graphics panel, int cellSize, int offsetX, int offsetY) {
        Color color = panel.getColor();
        panel.setColor(grid.getColor());

        int lastY = offsetY + getGridSizeHeight(grid, cellSize) - 1;
        int x = offsetX;
        for (int i = 0; i < grid.getWidthCells() + 1; i++) {
            panel.drawLine(x, offsetY, x, lastY);
            x += cellSize + 1;
        }
        int lastX = offsetX + getGridSizeWidth(grid, cellSize) - 1;
        int y = offsetY;
        for (int i = 0; i < grid.getHeightCells() + 1; i++) {
            panel.drawLine(offsetX, y, lastX, y);
            y += cellSize + 1;
        }
        panel.setColor(color);
    }

    @Override
    public void imageChanged(BufferedImage image) {
        if (image != null) {
            imageScaleCoefficient =
                Math.max(
                    MIN_IMAGE_SCALE_COEFFICIENT,
                    Math.min(
                        MAX_IMAGE_SCALE_COEFFICIENT,
                        Math.min(
                            imagePanel.getWidth() * NO_IMAGE_SCALE_COEFFICIENT / image.getWidth(),
                            imagePanel.getHeight() * NO_IMAGE_SCALE_COEFFICIENT / image.getHeight())));
            imageOffsetX = (imagePanel.getWidth() - getImageWidth(image)) / 2;
            imageOffsetY = 0;
        }
        repaintImage();
    }

    @Override
    public void gridChanged(Grid grid) {
        repaintImage();
    }
}
